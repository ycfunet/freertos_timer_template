#include "stdio.h"
#include "pico/stdlib.h"
#include "FreeRTOS.h"
#include "task.h"

#include "runner.h"
#include "ctimer.h"

int main() {
    BaseType_t ret;

    Runner *runnerA = new Runner(1);
    Runner *runnerB = new Runner(2);
    CTimer *timer1 = new CTimer("runnerA", runnerA);
    CTimer *timer2 = new CTimer("runnerB", runnerB);
    timer1->SetTimer(5, true);
    timer2->SetTimer(4, true);
    timer1->Start();
    timer2->Start();

    vTaskStartScheduler();

    while(1)
    {
        configASSERT(0);    /* We should never get here */
    }

}
