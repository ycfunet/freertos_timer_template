#ifndef RUNNER_H
#define RUNNER_H

#include "thread.h"

class Runner : public Thread
{
public:
    Runner(int id);
    void run();

private:
    int my_id;

};

#endif // RUNNER_H
