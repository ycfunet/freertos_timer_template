#ifndef CTIMER_H
#define CTIMER_H

#include "thread.h"
#include "FreeRTOS.h"
#include "timers.h"

class CTimer
{
public:
    CTimer(char *timer_name, Thread *run_thread);
    void SetTimer(uint32_t time_ticks, bool loop = false);
    void DelTimer();
    bool Start();
    void Stop();

    static void timer_callback(TimerHandle_t timer_handler) {
        Thread *thread = (Thread *)pvTimerGetTimerID(timer_handler);
        thread->run();
    }

private:
    char *timer_name;
    TimerHandle_t timer_handle;
    bool is_created;

    Thread *run_thread;
};

#endif // CTIMER_H
