#include "ctimer.h"

CTimer::CTimer(char *timer_name, Thread *run_thread)
{
    this->timer_name = timer_name;
    this->run_thread = run_thread;
    this->timer_handle = NULL;
    this->is_created = false;
}

void CTimer::SetTimer(uint32_t time_ticks, bool loop)
{
    UBaseType_t auto_reload;

    if(loop == true)
        auto_reload = pdTRUE;
    else
        auto_reload = pdFALSE;

    this->timer_handle = xTimerCreate(this->timer_name, time_ticks, auto_reload, (void *)this->run_thread, CTimer::timer_callback);
    this->is_created = true;
}

void CTimer::DelTimer()
{
    if(this->timer_handle)
        xTimerDelete(this->timer_handle, 0);
}

bool CTimer::Start()
{
    if(this->timer_handle)
    {
        BaseType_t ret = xTimerStart(this->timer_handle, 0);
        if(ret == pdPASS)
            return true;
        else
            return false;
    }
    else
        return false;
}

void CTimer::Stop()
{
    if(this->timer_handle)
        xTimerStop(this->timer_handle, 0);
}
